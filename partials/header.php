<nav class="navbar navbar-inverse">
	<div class="container-fluid">
	  <div class="navbar-header">
	    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#MyNavbar">
	      <span class="icon-bar"></span>
	      <span class="icon-bar"></span>
	      <span class="icon-bar"></span>
	    </button>
	    <a class="navbar-brand" href="#"><img src="img/logo.png"></a>
	  </div>
	  <div class="collapse navbar-collapse" id="MyNavbar">
	    <ul class="nav navbar-nav navbar-right">
	      <li class="active"><a href="#">Home</a></li>
	      <li><a href="#">About</a></li>
	      <li><a href="#">Services</a></li>
	      <li><a href="#">Testimonials</a></li>
	      <li><a href="#">Contact</a></li>
	    </ul>
	  </div>
	</div>
</nav>