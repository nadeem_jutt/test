<div class="container text-center">
	<h2>What We're Using</h2>
	<div class="row">
	  <div class="col-sm-4">
	    <img src="img/html5.png" id="icon">
	    <h4>HTML5</h4>
	  </div>
	  <div class="col-sm-4">
	    <img src="img/bootstrap.png" id="icon">
	    <h4>Bootstrap</h4>
	  </div>
	  <div class="col-sm-4">
	    <img src="img/css3.png" id="icon">
	    <h4>CSS3</h4>
	  </div>
	</div>
</div>