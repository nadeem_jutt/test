<div class="container">
	<div class="row">
	  <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
	    <h4>Built with Sass</h4>
	    <p>aaaa bbbbb ccccc ddddddd eeeee ff gggg hhhhhhhh iiiiiiiiii jjjjjj kkkkkk lllllll mm n o p
	      ppppp qqq rrrrr sss tttttt uuuuuu vv w wwww xxxxxxx yyyyy zzz 
	      aaaa bbbbb ccccc ddddddd eeeee ff gggg hhhhhhhh iiiiiiiiii jjjjjj kkkkkk lllllll mm n o p
	      ppppp qqq rrrrr sss tttttt uuuuuu vv w wwww xxxxxxx yyyyy zzz .
	    </p>
	  </div>
	   <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
	    <img src="img/sass.png" class="img-responsive">
	  </div>
	  <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
	    <h4>And Less.</h4>
	    <p>aaaa bbbbb ccccc ddddddd eeeee ff gggg hhhhhhhh iiiiiiiiii jjjjjj kkkkkk lllllll mm n o p
	      ppppp qqq rrrrr sss tttttt uuuuuu vv w wwww xxxxxxx yyyyy zzz 
	      aaaa bbbbb ccccc ddddddd eeeee ff gggg hhhhhhhh iiiiiiiiii jjjjjj kkkkkk lllllll mm n o p
	      ppppp qqq rrrrr sss tttttt uuuuuu vv w wwww xxxxxxx yyyyy zzz .
	    </p>
	  </div>
	   <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
	    <img src="img/less.png" class="img-responsive">
	  </div>
	</div>
</div>