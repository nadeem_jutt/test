<div id="myCarousel" class="carousel slide" data-ride="carousel">
	<ol class="carousel-indicators">
	  <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
	  <li data-target="#myCarousel" data-slide-to="1"></li>
	  <li data-target="#myCarousel" data-slide-to="2"></li>
	</ol>
	<div class="carousel-inner" role="listbox">
	  <div class="item active">
	    <img src="img/mountains.png">
	    <div class="carousel-caption">
	      <h1>Get To Know Bootstrap</h1>
	      <br>
	      <button type="button" class="btn btn-default">Get Started</button>
	    </div>
	  </div>
	  <div class="item">
	    <img src="img/snow.png">
	  </div>
	  <div class="item">
	    <img src="img/red.png">
	  </div>
	</div>

	<!-- Slider Control -->
	<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
	  <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
	  <span class="sr-only">Previous</span>
	</a>
	<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
	  <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
	  <span class="sr-only">Next</span>
	</a>
</div>